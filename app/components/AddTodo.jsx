import React from 'react';
import * as Redux from 'react-redux';
import * as actions from 'actions';

export class AddTodo extends React.Component{
  onSubmit(e) {
    e.preventDefault();
    var {dispatch} = this.props;
    var todoText = this.refs.todoText.value;

    if (todoText.length > 0) {
      this.refs.todoText.value = '';
      //this.props.onAddTodo(todoText);
      dispatch(actions.startAddTodo(todoText));
    } else {
      this.refs.todoText.focus();
    }
  }
  render() {
    return (
      <div className="container__footer">
        <form ref="form" onSubmit={this.onSubmit.bind(this)} className="todo-form">
          <input type="text" ref="todoText" placeholder="What you need to do ?"/>
          <button className="button expanded">Add Todo</button>
        </form>
      </div>
    );
  }
};

export default Redux.connect()(AddTodo);
