var React = require('react');
//var Todo = require('Todo');
import Todo from 'Todo';
var TodoAPI = require('TodoAPI');

import * as Redux from 'react-redux';

export class TodoList extends React.Component{
  render() {
    var {todos, showCompleted, searchText} = this.props;
    var renderTodos = () => {
      var filteredTodos = TodoAPI.filterTodos(todos, showCompleted, searchText);
      if (filteredTodos.length === 0) {
        return (
          <p className="container__message">Nothing to do</p>
        );
      }
      return filteredTodos.map( (todo) => {
        return (
          <Todo key={todo.id} {...todo}/>
        );
      });
    };

    return (
      <div>
        {renderTodos()}
      </div>
    );
  }
};

export default Redux.connect(
  (state) => {
    return state;
  }
)(TodoList);
