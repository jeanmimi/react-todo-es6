var $ = require('jquery');

module.exports = {

  filterTodos: function(todos, showCompleted, searchText) {
    var filteredTodos = todos;

    //filter by showCompleted
    filteredTodos = filteredTodos.filter( (todo) => {
      return !todo.completed || showCompleted;
    });

    //filter by searchText
    if (searchText && searchText.length > 0) {

      filteredTodos = filteredTodos.filter( (todo) => {
        var lowerText = todo.text.toLowerCase();
        return (lowerText.indexOf(searchText.toLowerCase()) != -1);
      });
    }

    //sort todos with non-completed first
    filteredTodos.sort( (a,b) => {
      if (!a.completed && b.completed) {
        return -1; //a before b
      } else if (a.completed && !b.completed) {
        return 1; //b before a
      } else {
        return 0;
      }
    });

    return filteredTodos;
  }
};
